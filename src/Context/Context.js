import React, { createContext, useContext, useReducer } from "react";
import PropTypes from "prop-types";
import { personReducer } from "./Reducers";

const Person = createContext();

function Context({ children }) {
  const [state, dispatch] = useReducer(personReducer, {
    persons: [],
    addFormDisplay: false,
    editFormDisplay: false,
    currentEditPerson: {},
    searchQuery: "",
    personsPerPage: 5,
  });

  return (
    <Person.Provider value={{ state, dispatch }}>{children}</Person.Provider>
  );
}

Context.propTypes = {
  children: PropTypes.node,
};

export default Context;

export const PersonState = () => {
  return useContext(Person);
};
