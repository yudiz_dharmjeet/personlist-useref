export const personReducer = (state, action) => {
  switch (action.type) {
    case "FETCH_DATA":
      return { ...state, persons: action.payload };

    case "DELETE_PERSON":
      return {
        ...state,
        persons: state.persons.filter(
          (person) => person.login.uuid !== action.payload
        ),
      };

    case "ADD_DISPLAY_FORM_TOGGLE":
      return {
        ...state,
        addFormDisplay: !state.addFormDisplay,
      };

    case "CLOSE_EDIT_FORM_WHEN_CLICK_ADD_FORM":
      return {
        ...state,
        editFormDisplay:
          state.editFormDisplay === true
            ? (state.editFormDisplay = false)
            : state.editFormDisplay,
      };

    case "CLOSE_ADD_FORM":
      return {
        ...state,
        addFormDisplay: false,
      };

    case "EDIT_DISPLAY_FORM_TOGGLE":
      return {
        ...state,
        currentEditPerson: state.persons.find(
          (person) => person.login.uuid === action.payload
        ),
        editFormDisplay: true,
      };

    case "CLOSE_EDIT_FORM":
      return {
        ...state,
        editFormDisplay: false,
      };

    case "ADD_PERSON":
      return {
        ...state,
        persons: [
          {
            login: { uuid: new Date().valueOf() },
            name: { first: action.payload.name },
            dob: { age: action.payload.age },
            phone: action.payload.phone,
          },
          ...state.persons,
        ],
      };

    case "EDIT_PERSON":
      return {
        ...state,
        persons: [
          {
            login: { uuid: new Date().valueOf() },
            name: { first: action.payload.name },
            dob: { age: action.payload.age },
            phone: action.payload.phone,
          },
          ...state.persons.filter(
            (person) => person.login.uuid !== action.payload.id
          ),
        ],
      };

    case "SORT_BY_AGE":
      return {
        ...state,
        sort: action.payload,
      };

    case "FILTER_BY_SEARCH":
      return {
        ...state,
        searchQuery: action.payload,
      };

    case "CHANGE_PERSONS_PER_PAGE":
      return {
        ...state,
        personsPerPage: action.payload,
      };

    default:
      return state;
  }
};
