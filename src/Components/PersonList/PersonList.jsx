import React, { useEffect, useState } from "react";
import { PersonState } from "../../Context/Context";
import Pagination from "../Pagination/Pagination";

import "./PersonList.scss";

const url = "https://randomuser.me/api/?results=40";

function PersonList() {
  const { state, dispatch } = PersonState();

  const [currentPage, setCurrentPage] = useState(1);
  const [personsPerPage, setPersonsPerPage] = useState(5);

  useEffect(() => {
    setPersonsPerPage(Number(state.personsPerPage));
  }, [state.personsPerPage]);

  async function getData() {
    const response = await fetch(url);
    const persons = await response.json();
    dispatch({ type: "FETCH_DATA", payload: persons.results });
  }

  useEffect(() => {
    getData();
  }, [url]);

  function handleEditButton(id) {
    dispatch({ type: "EDIT_DISPLAY_FORM_TOGGLE", payload: id });
    dispatch({ type: "CLOSE_ADD_FORM" });
  }

  function handleDeleteButton(person) {
    if (confirm("Are you sure you want to delete this person ?")) {
      dispatch({
        type: "DELETE_PERSON",
        payload: person.login.uuid,
      });
    }
    dispatch({ type: "CLOSE_ADD_FORM" });
    dispatch({ type: "CLOSE_EDIT_FORM" });
  }

  const transformPersons = () => {
    let newPersons = state.persons;

    if (state.sort) {
      newPersons = newPersons.sort((a, b) =>
        state.sort === "lowToHigh"
          ? a.dob.age - b.dob.age
          : b.dob.age - a.dob.age
      );
    }

    if (state.searchQuery) {
      newPersons = newPersons.filter((person) => {
        return person.name.first
          .toLowerCase()
          .includes(state.searchQuery.toLowerCase());
      });
    }

    return newPersons;
  };

  // Get Current Persons
  const totalPersons = transformPersons().length;
  const indexOfLastPerson = currentPage * personsPerPage;
  const indexOfFirstPerson = indexOfLastPerson - personsPerPage;
  const currentPersons =
    state.searchQuery == ""
      ? transformPersons().slice(indexOfFirstPerson, indexOfLastPerson)
      : transformPersons();

  // Change Page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <div className="person-list">
      <div className="container">
        {state.persons.length == 0 ? (
          <h3 style={{ fontSize: "30px", color: "white" }}>Loading...</h3>
        ) : (
          <>
            <table>
              <tbody>
                <tr>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Phone Number</th>
                  <th>Actions</th>
                </tr>
                {currentPersons.length == 0 ? (
                  <tr style={{ fontSize: "30px", color: "white" }}>
                    <td colSpan={4}>There is no such data...</td>
                  </tr>
                ) : (
                  currentPersons.map((person) => {
                    return (
                      <tr key={person.login.uuid}>
                        <td>{person.name.first}</td>
                        <td>{person.dob.age}</td>
                        <td>{person.phone}</td>
                        <td>
                          <button
                            className="btn"
                            style={{ background: "green", color: "white" }}
                            onClick={() => handleEditButton(person.login.uuid)}
                          >
                            Edit
                          </button>
                          <button
                            className="btn"
                            style={{
                              marginLeft: "8px",
                              background: "red",
                              color: "white",
                            }}
                            onClick={() => handleDeleteButton(person)}
                          >
                            Delete
                          </button>
                        </td>
                      </tr>
                    );
                  })
                )}
              </tbody>
            </table>
            {!state.searchQuery ? (
              <Pagination
                personsPerPage={personsPerPage}
                totalPersons={totalPersons}
                paginate={paginate}
              />
            ) : (
              <div style={{ marginTop: "20px" }}></div>
            )}
          </>
        )}
      </div>
    </div>
  );
}

export default PersonList;
