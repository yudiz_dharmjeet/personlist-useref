import Header from "./Header/Header";
import SearchForm from "./SearchForm/SearchForm";
import PersonList from "./PersonList/PersonList";
import AddForm from "./AddForm/AddForm";
import EditForm from "./EditForm/EditForm";
import SearchFormInput from "./SearchFormInput/SearchFormInput";
import Pagination from "./Pagination/Pagination";

export {
  Header,
  SearchForm,
  PersonList,
  AddForm,
  EditForm,
  SearchFormInput,
  Pagination,
};
