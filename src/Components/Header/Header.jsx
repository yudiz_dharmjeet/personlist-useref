import React from "react";
import { SearchForm } from "..";
import { PersonState } from "../../Context/Context";
import "./Header.scss";

function Header() {
  const { dispatch } = PersonState();

  function handleAddPersonButton() {
    dispatch({ type: "ADD_DISPLAY_FORM_TOGGLE" });
    dispatch({ type: "CLOSE_EDIT_FORM_WHEN_CLICK_ADD_FORM" });
  }

  return (
    <header className="header">
      <div className="container">
        <SearchForm />
        <div className="sort_section">
          <p>Sort by Age : </p>
          <input
            type="radio"
            id="accending"
            name="order"
            value="accending"
            onChange={() =>
              dispatch({
                type: "SORT_BY_AGE",
                payload: "lowToHigh",
              })
            }
          />
          <label htmlFor="accending">Accending</label>
          <input
            type="radio"
            id="decending"
            name="order"
            value="decending"
            onChange={() =>
              dispatch({
                type: "SORT_BY_AGE",
                payload: "highToLow",
              })
            }
          />
          <label htmlFor="decending">Decending</label>
        </div>
        <button className="btn" onClick={() => handleAddPersonButton()}>
          Add Person
        </button>
      </div>
    </header>
  );
}

export default Header;
