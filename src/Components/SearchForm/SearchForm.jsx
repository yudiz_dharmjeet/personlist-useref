import React, { useRef } from "react";
import { PersonState } from "../../Context/Context";
import SearchFormInput from "../SearchFormInput/SearchFormInput";
import "./SearchForm.scss";

function SearchForm() {
  const { dispatch } = PersonState();

  // const [sQuery, setSQuery] = useState("");

  const query = useRef(null);

  function handleSearchButton(e) {
    e.preventDefault();
    dispatch({ type: "FILTER_BY_SEARCH", payload: query.current.value });
    query.current.value = "";
  }

  return (
    <form className="search">
      <SearchFormInput ref={query} />
      <button className="btn" onClick={handleSearchButton}>
        Search
      </button>
    </form>
  );
}

export default SearchForm;
