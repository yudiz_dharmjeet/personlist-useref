import React, { useRef } from "react";
import { PersonState } from "../../Context/Context";
import "./AddForm.scss";

function AddForm() {
  const { dispatch } = PersonState();

  const name = useRef(null);
  const age = useRef(null);
  const phone = useRef(null);

  function handleAddForm(e) {
    e.preventDefault();
    dispatch({
      type: "ADD_PERSON",
      payload: {
        name: name.current.value,
        age: age.current.value,
        phone: phone.current.value,
      },
    });
    name.current.value = "";
    age.current.value = "";
    phone.current.value = "";
    dispatch({
      type: "ADD_DISPLAY_FORM_TOGGLE",
    });
  }

  return (
    <div className="addform">
      <div className="container">
        <form className="addform__form" onSubmit={handleAddForm}>
          <input type="text" ref={name} placeholder="Enter your name.." />
          <input type="number" ref={age} placeholder="Enter your age.." />
          <input type="number" ref={phone} placeholder="Enter your phone.." />
          <button className="btn">Submit</button>
        </form>
      </div>
    </div>
  );
}

export default AddForm;
