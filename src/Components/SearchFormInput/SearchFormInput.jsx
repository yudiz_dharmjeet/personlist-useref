import React, { forwardRef } from "react";
import "./SearchFormInput.scss";

function SearchFormInput(props, ref) {
  return (
    <input
      type="text"
      placeholder="Search by Name..."
      className="search__input"
      ref={ref}
    />
  );
}

export default forwardRef(SearchFormInput);
