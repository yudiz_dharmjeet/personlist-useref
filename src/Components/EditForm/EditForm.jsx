import React, { useEffect, useState } from "react";
import { PersonState } from "../../Context/Context";

import "./EditForm.scss";

function EditForm() {
  const { state, dispatch } = PersonState();

  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [phone, setPhone] = useState("");

  useEffect(() => {
    setName(state.currentEditPerson?.name.first || "");
    setAge(state.currentEditPerson?.dob.age || 0);
    setPhone(state.currentEditPerson?.phone || "");
  }, [state.currentEditPerson]);

  function handleEditForm(e) {
    e.preventDefault();
    dispatch({
      type: "EDIT_PERSON",
      payload: {
        id: state.currentEditPerson.login.uuid,
        name: name,
        age: age,
        phone: phone,
      },
    });
    setName("");
    setAge("");
    setPhone("");
    dispatch({
      type: "CLOSE_EDIT_FORM",
    });
  }

  return (
    <div className="editform">
      <div className="container">
        <form className="editform__form" onSubmit={handleEditForm}>
          <input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Enter your name.."
          />
          <input
            type="number"
            value={Number(age)}
            onChange={(e) => setAge(e.target.value)}
            placeholder="Enter your age.."
          />
          <input
            type="text"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            placeholder="Enter your phone.."
          />
          <button className="btn">Edit</button>
        </form>
      </div>
    </div>
  );
}

export default EditForm;
