import React from "react";
import PropTypes from "prop-types";
import "./Pagination.scss";
import { PersonState } from "../../Context/Context";

function Pagination({ personsPerPage, totalPersons, paginate }) {
  const { dispatch } = PersonState();

  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPersons / personsPerPage); i++) {
    pageNumbers.push(i);
  }

  function handleChange(e) {
    dispatch({
      type: "CHANGE_PERSONS_PER_PAGE",
      payload: Number(e.target.value),
    });
  }

  return (
    <div className="bottom__section">
      <ul className="pagination">
        {pageNumbers.map((number) => (
          <li key={number} className="page-item">
            <a onClick={() => paginate(number)}>{number}</a>
          </li>
        ))}
      </ul>
      <div className="personperpage_section">
        <h5>Persons Per Page : </h5>
        <select
          name="personperpages"
          id="personperpages"
          onChange={handleChange}
        >
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="15">15</option>
          <option value="20">20</option>
        </select>
      </div>
    </div>
  );
}

Pagination.propTypes = {
  personsPerPage: PropTypes.number,
  totalPersons: PropTypes.number,
  paginate: PropTypes.func,
};

export default Pagination;
