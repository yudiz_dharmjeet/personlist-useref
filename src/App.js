import React from "react";
import { AddForm, EditForm, Header, PersonList } from "./Components";
import { PersonState } from "./Context/Context";

function App() {
  const { state } = PersonState();

  return (
    <>
      <Header />
      {state.addFormDisplay && <AddForm />}
      {state.editFormDisplay && <EditForm />}
      <PersonList />
    </>
  );
}

export default App;
